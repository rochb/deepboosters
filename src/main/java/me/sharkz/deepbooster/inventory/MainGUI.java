package me.sharkz.deepbooster.inventory;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.boosters.Booster;
import me.sharkz.deepbooster.core.exceptions.InventoryOpenException;
import me.sharkz.deepbooster.core.inventories.VInventory;
import me.sharkz.deepbooster.core.inventories.utils.InventoryResult;
import me.sharkz.deepbooster.core.utils.builders.ItemBuilder;
import me.sharkz.deepbooster.core.utils.builders.TimerBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainGUI extends VInventory {

    public MainGUI() {
        createInventory(Lang.GUI_NAME, Lang.GUI_SIZE);
    }

    @Override
    public InventoryResult openInventory(JavaPlugin main, Player player, int page, Object... args) throws InventoryOpenException {
        // TEMP
        ItemBuilder right = new ItemBuilder(Material.ANVIL, Lang.RIGHT_ANVIL_NAME);
        // PERM
        ItemBuilder left = new ItemBuilder(Material.ANVIL, Lang.LEFT_ANVIL_NAME);

        List<Booster> tmpBoosters = DeepBooster.getBoostersManager().getActiveBoosters(player).stream().filter(booster -> booster.getTime() < 999999999).collect(Collectors.toList());
        List<Booster> permBoosters = DeepBooster.getBoostersManager().getActiveBoosters(player).stream().filter(booster -> booster.getTime() == 999999999).collect(Collectors.toList());

        addItem(31, new ItemBuilder(Material.ARROW, Lang.GUI_CLOSE_NAME).setLore(Lang.GUI_CLOSE_LORE.toArray(new String[]{})).build())
                .setClick(e -> player.closeInventory());

        addFiller();

        new BukkitRunnable() {
            @Override
            public void run() {
                List<String> rightLore = new ArrayList<>(Lang.RIGHT_ANVIL_LORE);
                rightLore.add(" ");
                rightLore.add(Lang.GUI_ACTIVE_BOOSTERS);
                List<String> leftLore = new ArrayList<>(Lang.LEFT_ANVIL_LORE);
                leftLore.add(" ");
                leftLore.add(Lang.GUI_ACTIVE_BOOSTERS);


                if (tmpBoosters.size() > 0)
                    tmpBoosters.forEach(booster -> rightLore.add(ChatColor.WHITE + "- " + (1 + (booster.getPercent() / (float) 100)) + "x " + booster.getType().getName() + " Boost &8[" + TimerBuilder.getStringTime(booster.getRemainingSeconds()) + "]"));
                else
                    rightLore.add(Lang.GUI_NONE_STRING);

                if (permBoosters.size() > 0)
                    permBoosters.forEach(booster -> leftLore.add(ChatColor.WHITE + "- " + (1 + (booster.getPercent() / (float) 100)) + "x " + booster.getType().getName() + " Boost"));
                else
                    leftLore.add(Lang.GUI_NONE_STRING);

                right.setLore(rightLore.toArray(new String[]{}));
                left.setLore(leftLore.toArray(new String[]{}));
                addItem(12, right.build());
                addItem(14, left.build());

                player.updateInventory();
            }
        }.runTaskTimer(DeepBooster.getInstance(), 0L, 20L);
        return InventoryResult.SUCCESS;
    }

    @Override
    protected void onClose(InventoryCloseEvent event, JavaPlugin plugin, Player player) {

    }

    @Override
    protected void onDrag(InventoryDragEvent event, JavaPlugin plugin, Player player) {

    }
}
