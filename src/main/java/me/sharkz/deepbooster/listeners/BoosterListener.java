package me.sharkz.deepbooster.listeners;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.boosters.BoosterType;
import me.sharkz.deepbooster.boosters.BoostersManager;
import me.sharkz.deepbooster.core.listeners.ListenerAdapter;
import net.brcdev.shopgui.event.ShopPreTransactionEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class BoosterListener extends ListenerAdapter {

    protected BoostersManager bm = DeepBooster.getBoostersManager();

    @Override
    protected void onPlayerXp(PlayerExpChangeEvent e, Player player, int amount) {
        bm.getActiveBoosters(player, BoosterType.XP).forEach(booster -> {
            float boostedAmount = (booster.getPercent() / (float) 100) * amount;
            e.setAmount((int) boostedAmount + amount);
            actionMessage(player, Lang.msg.get("xp_boost").replaceAll("%bonus%", String.valueOf(boostedAmount)));
        });
    }

    @Override
    protected void onPlayerXpMCMMO(McMMOPlayerXpGainEvent e, Player player, float rawXpGained) {
        bm.getActiveBoosters(player, BoosterType.MCMMO).forEach(booster -> {
            float boostedAmount = (booster.getPercent() / (float) 100) * rawXpGained;
            e.setRawXpGained(boostedAmount + rawXpGained);
            actionMessage(player, Lang.msg.get("mcmmo_boost").replaceAll("%bonus%", String.valueOf(boostedAmount)));
        });
    }

    @Override
    protected void onShopSell(ShopPreTransactionEvent e, Player player, int amount, double price) {
        bm.getActiveBoosters(player, BoosterType.SHOP).forEach(booster -> {
            double boostedAmount = (booster.getPercent() / (float) 100) * price;
            e.setPrice(boostedAmount + price);
            actionMessage(player, Lang.msg.get("shop_boost").replaceAll("%bonus%", String.valueOf(boostedAmount)));
        });
    }

}
