package me.sharkz.deepbooster.core.commands;


import org.bukkit.plugin.java.JavaPlugin;

import java.util.function.BiConsumer;

public class ZCommand extends VCommand {

	private BiConsumer<VCommand, JavaPlugin> command;

	@Override
	public CommandType perform(JavaPlugin plugin) {
		
		if (command != null){
			command.accept(this, plugin);
		}

		return CommandType.SUCCESS;
	}

	public VCommand setCommand(BiConsumer<VCommand, JavaPlugin> command) {
		this.command = command;
		return this;
	}

	public VCommand sendHelp(CommandManager manager, String command) {
		this.command = (cmd, plugin) -> manager.sendHelp(command, cmd.getSender());
		return this;
	}

}
