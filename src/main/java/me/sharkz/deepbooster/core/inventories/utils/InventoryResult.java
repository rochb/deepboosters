package me.sharkz.deepbooster.core.inventories.utils;

public enum InventoryResult {
	SUCCESS,
	ERROR,
	DEFAULT,
}
