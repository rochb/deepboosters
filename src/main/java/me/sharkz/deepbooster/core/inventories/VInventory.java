package me.sharkz.deepbooster.core.inventories;

import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.core.exceptions.InventoryOpenException;
import me.sharkz.deepbooster.core.inventories.utils.InventoryResult;
import me.sharkz.deepbooster.core.inventories.utils.ItemButton;
import me.sharkz.deepbooster.core.utils.LUtils;
import me.sharkz.deepbooster.core.utils.builders.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public abstract class VInventory extends LUtils implements Cloneable{

	protected int id;
	protected JavaPlugin plugin;
	protected Map<Integer, ItemButton> items = new HashMap<>();
	protected Player player;
	protected int page;
	protected Object[] args;
	protected Inventory inventory;
	protected String guiName;
	protected boolean disableClick = true;

	/**
	 * Id de l'inventaire
	 * @param id
	 * @return
	 */
	public VInventory setId(int id) {
		this.id = id;
		return this;
	}

	public int getId() {
		return id;
	}

	/**
	 * Permet de cr�er l'inventaire
	 * @param name
	 * @return this
	 */
	protected VInventory createInventory(String name) {
		return createInventory(name, 54);
	}

	/**
	 * Permet de cr�er l'inventaire
	 * @param name
	 * @param size
	 * @return this
	 */
	protected VInventory createInventory(String name, int size) {
		guiName = color(name);
		this.inventory = Bukkit.createInventory(null, size, color(name));
		return this;
	}

	private void createDefaultInventory(){
		if (inventory == null) inventory = Bukkit.createInventory(null, 54, color("&7Default Inventory"));
	}
	
	/**
	 * Ajout d'un item
	 * @param slot
	 * @return
	 */
	public ItemButton addItem(int slot, Material material, String name) {
		return addItem(slot, new ItemBuilder(material, color(name)).build());
	}
	
	public ItemButton addItem(int slot, ItemStack item) {
		// Pour �viter les erreurs, on cr�e un inventaire
		createDefaultInventory();
		
		ItemButton button = new ItemButton(item);
		this.items.put(slot, button);
		this.inventory.setItem(slot, item);
		return button;
	}

	protected void addPreviousButton(InventoryManager manager, int slot, int invId){
		addItem(slot, Material.ARROW, "&6» &ePage précèdente")
				.setClick(event -> createInventory(manager, player, getId(), getPage() - 1, args));
	}

	protected void addNextButton(InventoryManager manager, int slot, int invId){
		addItem(slot, Material.ARROW, "&6» &ePage suivante")
				.setClick(event -> createInventory(manager, player, getId(), getPage() + 1, args));
	}

	protected void addFiller(){
		for (int slot = 0; slot < inventory.getSize(); slot++)
			if (inventory.getItem(slot) == null)
				addItem(slot, new ItemBuilder(Lang.GUI_FILLER, 1,  Lang.GUI_FILLER_NAME).setLore(Lang.GUI_FILLER_LORE.toArray(new String[]{})).build());
	}

	/**
	 * Permet de retirer un item de la liste des items
	 * @param slot
	 */
	public void removeItem(int slot) {
		this.items.remove(slot);
	}

	/**
	 * Permet de supprimer tous les items
	 */
	public void clearItem() {
		this.items.clear();
	}

	/**
	 * Permet de r�cup�rer tous les items
	 * @return
	 */
	public Map<Integer, ItemButton> getItems() {
		return items;
	}

	/**
	 * Si le click dans l'inventaire est d�sactiv� (se qui est par default) alors il va retourner vrai
	 * @return vrai ou faux
	 */
	public boolean isDisableClick() {
		return disableClick;
	}

	/**
	 * Changer le fait de pouvoir cliquer dans l'inventaire
	 * @param disableClick
	 */
	protected void setDisableClick(boolean disableClick) {
		this.disableClick = disableClick;
	}

	/**
	 * Permet de r�cup�rer le joueur
	 * @return player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Permet de r�cup�rer la page
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @return the args
	 */
	public Object[] getObjets() {
		return args;
	}

	/**
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}

	/**
	 * @return the guiName
	 */
	public String getGuiName() {
		return guiName;
	}

	protected InventoryResult preOpenInventory(JavaPlugin main, Player player, int page, Object... args) throws InventoryOpenException {
		this.page = page;
		this.args = args;
		this.player = player;
		this.plugin = main;
		player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10f, 1f);
		return openInventory(main, player, page, args);
	}
	
	public abstract InventoryResult openInventory(JavaPlugin main, Player player, int page, Object... args) throws InventoryOpenException;

	protected abstract void onClose(InventoryCloseEvent event, JavaPlugin plugin, Player player);

	protected abstract void onDrag(InventoryDragEvent event, JavaPlugin plugin, Player player);

	@Override
	protected VInventory clone()  {
		try {
			return (VInventory) getClass().newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
