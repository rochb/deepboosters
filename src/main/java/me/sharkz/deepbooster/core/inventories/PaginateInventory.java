package me.sharkz.deepbooster.core.inventories;

import me.sharkz.deepbooster.core.exceptions.InventoryOpenException;
import me.sharkz.deepbooster.core.inventories.utils.InventoryResult;
import me.sharkz.deepbooster.core.inventories.utils.ItemButton;
import me.sharkz.deepbooster.core.inventories.utils.Pagination;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class PaginateInventory<T> extends VInventory {

    protected final String inventoryName;
    protected final int inventorySize;
    private List<T> collections;
    private int paginationSize = 45;
    private int nextSlot = 50;
    private int previousSlot = 48;
    private int defaultSlot = 0;
    private boolean isReverse = false;

    public PaginateInventory(String inventoryName, int inventorySize) {
        super();
        this.inventoryName = inventoryName;
        this.inventorySize = inventorySize;
    }

    @Override
    public InventoryResult openInventory(JavaPlugin plugin, Player player, int page, Object... args) throws InventoryOpenException {
        preOpenInventory();

        super.createInventory(inventoryName.replace("%mp%", String.valueOf(getMaxPage(collections))).replace("%p%",
                String.valueOf(page)), inventorySize);

        Pagination<T> pagination = new Pagination<>();
        AtomicInteger slot = new AtomicInteger(defaultSlot);

        List<T> tmpList = isReverse ? pagination.paginateReverse(collections, paginationSize, page)
                : pagination.paginate(collections, paginationSize, page);

        tmpList.forEach(tmpItem -> {
            ItemButton button = addItem(slot.getAndIncrement(), buildItem(tmpItem));
            button.setClick((event) -> onClick(tmpItem, button));
        });

        postOpenInventory();

        return InventoryResult.SUCCESS;
    }

    protected void setCollections(List<T> collections) {
        this.collections = collections;
    }

    protected void setPaginationSize(int paginationSize) {
        this.paginationSize = paginationSize;
    }

    protected void setReverse(boolean isReverse) {
        this.isReverse = isReverse;
    }

    protected void setNextSlot(int nextSlot) {
        this.nextSlot = nextSlot;
    }

    protected void setPreviousSlot(int previousSlot) {
        this.previousSlot = previousSlot;
    }

    protected void setDefaultSlot(int defaultSlot) {
        this.defaultSlot = defaultSlot;
    }

    public abstract ItemStack buildItem(T object);

    public abstract void onClick(T object, ItemButton button);

    public abstract List<T> preOpenInventory();

    public abstract void postOpenInventory();
}
