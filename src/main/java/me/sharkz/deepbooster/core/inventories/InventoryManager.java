package me.sharkz.deepbooster.core.inventories;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.core.exceptions.InventoryAlreadyExistException;
import me.sharkz.deepbooster.core.exceptions.InventoryOpenException;
import me.sharkz.deepbooster.core.inventories.utils.InventoryResult;
import me.sharkz.deepbooster.core.inventories.utils.ItemButton;
import me.sharkz.deepbooster.core.listeners.ListenerAdapter;
import me.sharkz.deepbooster.core.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

public class InventoryManager extends ListenerAdapter {

    private final Map<Integer, VInventory> inventories = new HashMap<>();
    private final Map<Player, VInventory> playerInventories = new HashMap<>();

    public void addInventory(int id, VInventory inventory) {
        if (!inventories.containsKey(id)) inventories.put(id, inventory);
        else throw new InventoryAlreadyExistException("Inventory with id " + id + " is already registered !");
    }

    public void createInventory(int id, Player player, int page, Object... objects) {
        VInventory inventory = getInventory(id);
        if (inventory == null) {
            message(player, color("&eImpossible d'ouvrir ce menu car il est vide."), id);
            return;
        }
        VInventory clonedInventory = inventory.clone();

        if (clonedInventory == null) {
            message(player, color("&eImpossible d'ouvrir ce menu car il est vide."));
            return;
        }

        clonedInventory.setId(id);
        InventoryResult result = null;
        try {
            result = clonedInventory.preOpenInventory(plugin, player, page, objects);
        } catch (InventoryOpenException e) {
            message(player, color("&eImpossible d'ouvrir ce menu."), id);
        }
        if (result.equals(InventoryResult.SUCCESS)) {
            player.openInventory(clonedInventory.getInventory());
            playerInventories.put(player, clonedInventory);
        } else if (result.equals(InventoryResult.ERROR))
            message(player, color("&eImpossible d'ouvrir ce menu."), id);
    }

    public void createInventory(VInventory parent, Player player) {
        createInventory(parent.getId(), player, parent.getPage(), parent.getObjets());
    }

    @Override
    protected void onInventoryClick(InventoryClickEvent event, Player player) {
        if (event.getClickedInventory() == null) return;
        if (event.getWhoClicked() instanceof Player) {
            if (!exist(player)) return;
            VInventory gui = playerInventories.get(player);
            if (gui.getGuiName() == null || gui.getGuiName().length() == 0) {
                Logger.info("An error has occurred with the menu ! " + gui.getClass().getName());
                return;
            }
            event.getView();
            if (gui.getPlayer().equals(player) && colorReverse(event.getView().getTitle()).equals(colorReverse(gui.getGuiName()))) {
                if(gui.isDisableClick()) event.setCancelled(true);
                ItemButton button = gui.getItems().getOrDefault(event.getSlot(), null);
                if (button != null) button.onClick(event);
            }
        }
    }

    @Override
    protected void onInventoryClose(InventoryCloseEvent event, Player player) {
        if (!exist(player)) return;
        VInventory inventory = playerInventories.get(player);
        remove(player);
        inventory.onClose(event, plugin, player);
    }

    @Override
    protected void onInventoryDrag(InventoryDragEvent event, Player player) {
        if (event.getWhoClicked() instanceof Player) {
            if (!exist(player)) return;
            playerInventories.get(player).onDrag(event, plugin, player);
        }
    }

    public boolean exist(Player player) {
        return playerInventories.containsKey(player);
    }

    public void remove(Player player) {
        playerInventories.remove(player);
    }

    public VInventory getInventory(int id) {
        return inventories.getOrDefault(id, null);
    }

    /**
     * @param id
     */
    public void updateAllPlayer(int... id) {
        for (int currentId : id)
            updateAllPlayer(currentId);
    }

    /**
     * @param id
     */
    public void closeAllPlayer(int... id) {
        for (int currentId : id)
            closeAllPlayer(currentId);
    }

    /**
     * @param id
     */
    private void updateAllPlayer(int id) {
        Iterator<VInventory> iterator = this.playerInventories.values().stream().filter(inv -> inv.getId() == id)
                .collect(Collectors.toList()).iterator();
        while (iterator.hasNext()) {
            VInventory inventory = iterator.next();
            Bukkit.getScheduler().runTask(DeepBooster.getInstance(), () -> createInventory(inventory, inventory.getPlayer()));
        }
    }

    /**
     * @param id
     */
    private void closeAllPlayer(int id) {
        Iterator<VInventory> iterator = this.playerInventories.values().stream().filter(inv -> inv.getId() == id)
                .collect(Collectors.toList()).iterator();
        while (iterator.hasNext()) {
            VInventory inventory = iterator.next();
            inventory.getPlayer().closeInventory();
        }
    }
}