package me.sharkz.deepbooster.core.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.sharkz.deepbooster.DeepBooster;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class GsonManager {

    private final Gson gson;
    private final Persist persist;
    private final List<Saveable> savers = new ArrayList<>();

    public GsonManager(JavaPlugin plugin) {
        this.gson = DeepBooster.getGsonBuilder().create();
        this.persist = new Persist(plugin, this);
        if(!plugin.getDataFolder().exists()) plugin.getDataFolder().mkdir();
    }

    public GsonManager(JavaPlugin plugin, GsonBuilder gsonBuilder) {
        this.gson = gsonBuilder.create();
        this.persist = new Persist(plugin, this);
        if(!plugin.getDataFolder().exists()) plugin.getDataFolder().mkdir();
    }

    public void add(Saveable saveable){
        savers.add(saveable);
    }

    public void load(){
        savers.forEach(s -> s.load(persist));
    }

    public void save(){
        savers.forEach(s -> s.save(persist));
    }

    public Gson getGson() {
        return gson;
    }

    public Persist getPersist() {
        return persist;
    }

    public List<Saveable> getSavers() {
        return savers;
    }
}
