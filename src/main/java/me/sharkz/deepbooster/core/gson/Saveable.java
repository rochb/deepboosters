package me.sharkz.deepbooster.core.gson;

public interface Saveable {
	
	void save(Persist persist);
	void load(Persist persist);
}
