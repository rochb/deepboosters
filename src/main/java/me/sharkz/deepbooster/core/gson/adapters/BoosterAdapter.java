package me.sharkz.deepbooster.core.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.boosters.Booster;
import me.sharkz.deepbooster.boosters.BoosterType;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BoosterAdapter extends TypeAdapter<Booster> {

    private static final Type seriType = new TypeToken<Map<String, Object>>() {
    }.getType();

    //
    //    private final UUID uuid;
    //    private final long start;
    //    private final long time;
    //    private final int percent;
    //    public final BoosterType type;
    //
    private static final String UUID = "uuid";
    private static final String START = "start";
    private static final String TIME = "time";
    private static final String PERCENT = "percent";
    private static final String TYPE = "type";

    public BoosterAdapter() {}

    @Override
    public void write(JsonWriter jsonWriter, Booster Booster) throws IOException {
        if (Booster == null) {
            jsonWriter.nullValue();
            return;
        }
        jsonWriter.value(getRaw(Booster));
    }

    @Override
    public Booster read(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            System.out.println("NULL");
            return null;
        }
        System.out.println("OK");
        return fromRaw(jsonReader.nextString());
    }

    private String getRaw(Booster Booster) {
        Map<String, Object> serial = new HashMap<>();
        serial.put(UUID, Booster.getUuid());
        serial.put(START, Long.toString(Booster.getStart()));
        serial.put(TIME, Long.toString(Booster.getTime()));
        serial.put(PERCENT, Integer.toString(Booster.getPercent()));
        serial.put(TYPE, Booster.getType().name());
        return DeepBooster.getGsonBuilder().create().toJson(serial);
    }

    private Booster fromRaw(String raw) {
        Map<String, Object> keys = DeepBooster.getGsonBuilder().create().fromJson(raw, seriType);
        BoosterType type = BoosterType.valueOf(String.valueOf(keys.get(TYPE)).toUpperCase());
        return new Booster(java.util.UUID.fromString(String.valueOf(keys.get(UUID))), Long.parseLong((String) keys.get(START)), Long.parseLong((String) keys.get(TIME)), Integer.parseInt((String) keys.get(PERCENT)), type);
    }

}

