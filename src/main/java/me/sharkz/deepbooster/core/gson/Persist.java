package me.sharkz.deepbooster.core.gson;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.core.utils.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.lang.reflect.Type;

public class Persist {

    private final JavaPlugin plugin;
    private final GsonManager manager;

    public Persist(JavaPlugin plugin, GsonManager manager) {
        this.plugin = plugin;
        this.manager = manager;
    }

    // ------------------------------------------------------------ //
    // GET NAME - What should we call this type of object?
    // ------------------------------------------------------------ //

    public static String getName(Class<?> clazz) {
        return clazz.getSimpleName().toLowerCase();
    }

    public static String getName(Object o) {
        return getName(o.getClass());
    }

    public static String getName(Type type) {
        return getName(type.getClass());
    }

    // ------------------------------------------------------------ //
    // GET FILE - In which file would we like to store this object?
    // ------------------------------------------------------------ //

    public File getFile(String name) {
        return new File(plugin.getDataFolder(), name + ".json");
    }

    public File getFile(Class<?> clazz) {
        return getFile(getName(clazz));
    }

    public File getFile(Object obj) {
        return getFile(getName(obj));
    }

    public File getFile(Type type) {
        return getFile(getName(type));
    }


    // NICE WRAPPERS

    public <T> T loadOrSaveDefault(T def, Class<T> clazz) {
        return loadOrSaveDefault(def, clazz, getFile(clazz));
    }

    public <T> T loadOrSaveDefault(T def, Class<T> clazz, String name) {
        return loadOrSaveDefault(def, clazz, getFile(name));
    }

    public <T> T loadOrSaveDefault(T def, Class<T> clazz, File file) {
        if (!file.exists()) {
            DeepBooster.LOG.log("Creating default: " + file.getName(), Logger.LogType.SUCCESS);
            this.save(def, file);
            return def;
        }

        T loaded = this.load(clazz, file);

        if (loaded == null) {
            DeepBooster.LOG.log("Using default as I failed to load: " + file.getName(), Logger.LogType.WARNING);

            /*
             * Create new config backup
             * */

            File backup = new File(file.getPath() + "_bad");
            if (backup.exists()) backup.delete();
            DeepBooster.LOG.log("Backing up copy of bad file to: " + backup.getName(), Logger.LogType.WARNING);

            file.renameTo(backup);

            return def;
        } else
            DeepBooster.LOG.log(file.getName() + " loaded successfully !", Logger.LogType.SUCCESS);
        return loaded;
    }

    // SAVE

    public boolean save(Object instance) {
        return save(instance, getFile(instance));
    }

    public boolean save(Object instance, String name) {
        return save(instance, getFile(name));
    }

    public boolean save(Object instance, File file) {
        return DiscUtils.writeCatch(file, manager.getGson().toJson(instance));
    }

    // LOAD BY CLASS

    public <T> T load(Class<T> clazz) {
        return load(clazz, getFile(clazz));
    }

    public <T> T load(Class<T> clazz, String name) {
        return load(clazz, getFile(name));
    }

    public <T> T load(Class<T> clazz, File file) {
        String content = DiscUtils.readCatch(file);
        if (content == null) return null;


        try {
            return manager.getGson().fromJson(content, clazz);
        } catch (Exception ex) {    // output the error message rather than full stack trace; error parsing the file, most likely
            DeepBooster.LOG.log(ex.getMessage(), Logger.LogType.ERROR);
        }

        return null;
    }


    // LOAD BY TYPE
    public <T> T load(Type typeOfT, String name) {
        return (T) load(typeOfT, getFile(name));
    }

    public <T> T load(Type typeOfT, File file) {
        String content = DiscUtils.readCatch(file);
        if (content == null) return null;
        try {
            return (T) manager.getGson().fromJson(content, typeOfT);
        } catch (Exception ex) {    // output the error message rather than full stack trace; error parsing the file, most likely
            DeepBooster.LOG.log(ex.getMessage(), Logger.LogType.ERROR);
        }
        return null;
    }

}
