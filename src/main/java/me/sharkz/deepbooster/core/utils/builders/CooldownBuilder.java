package me.sharkz.deepbooster.core.utils.builders;

import me.sharkz.deepbooster.core.gson.Persist;
import me.sharkz.deepbooster.core.gson.Saveable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class CooldownBuilder implements Saveable {

	public static HashMap<String, HashMap<UUID, Long>> cooldowns = new HashMap<>();

	public static HashMap<UUID, Long> getCooldownMap(String s) {
		if (cooldowns.containsKey(s)) {
			return cooldowns.get(s);
		}
		return null;
	}

	public static void clear() {
		cooldowns.clear();
	}

	public static void createCooldown(String s) {
		if (!cooldowns.containsKey(s)) {
			cooldowns.put(s, new HashMap<>());
		}
	}

	public static void removeCooldown(String s, Player joueur) {
		if (!cooldowns.containsKey(s)) {
			throw new IllegalArgumentException("! Attention ! " + s + " n'existe pas.");
		}
		(cooldowns.get(s)).remove(joueur.getUniqueId());
	}

	public static void addCooldown(String s, Player joueur, int seconds){
		if (!cooldowns.containsKey(s)) cooldowns.put(s, new HashMap<>());
		long next = System.currentTimeMillis() + seconds * 1000L;
		(cooldowns.get(s)).put(joueur.getUniqueId(), next);
	}

	public static boolean isCooldown(String s, Player joueur) {
		return (cooldowns.containsKey(s)) && ((cooldowns.get(s)).containsKey(joueur.getUniqueId()))
				&& (System.currentTimeMillis() <= (cooldowns.get(s)).get(joueur.getUniqueId()));
	}

	public static long getCooldownPlayer(String s, Player joueur) {
		return (cooldowns.get(s)).getOrDefault(joueur.getUniqueId(), 0l) - System.currentTimeMillis();
	}

	public static String getCooldownAsString(String s, Player player) {
		return TimerBuilder.getStringTime(getCooldownPlayer(s, player) / 1000);
	}
	
	private static final transient CooldownBuilder i = new CooldownBuilder();

	@Override
	public void save(Persist persist) {
		persist.save(i, "cooldowns");
	}

	@Override
	public void load(Persist persist) {
		persist.loadOrSaveDefault(i, CooldownBuilder.class, "cooldowns");
	}
}
