package me.sharkz.deepbooster.core.utils.builders;

public class TimerBuilder {

    private static final String TIME_DAY = "%02d d %02d h %02d m %02d s";
    private static final String TIME_HOUR = "%02d h %02d m %02d s";
    private static final String TIME_HOUR_SIMPLE = "%02d:%02d:%02d";
    private static final String TIME_MINUTE = "%02d m %02d s";
    private static final String TIME_SECOND = "%02d s";

    public static String getFormatLongDays(long temps) {
        long totalSecs = temps / 1000L;
        return String.format(TIME_DAY,
                totalSecs / 86400L, totalSecs % 86400L / 3600L,
                totalSecs % 3600L / 60L, totalSecs % 60L);
    }

    public static String getFormatLongHours(long temps) {
        long totalSecs = temps / 1000L;
        return String.format(TIME_HOUR, totalSecs / 3600L,
                totalSecs % 3600L / 60L, totalSecs % 60L);
    }

    public static String getFormatLongHoursSimple(long temps) {
        long totalSecs = temps / 1000L;
        return String.format(TIME_HOUR_SIMPLE, totalSecs / 3600L,
                totalSecs % 3600L / 60L, totalSecs % 60L);
    }

    public static String getFormatLongMinutes(long temps) {
        long totalSecs = temps / 1000L;
        return String.format(TIME_MINUTE,
                totalSecs % 3600L / 60L, totalSecs % 60L);
    }

    public static String getFormatLongseconds(long temps) {
        long totalSecs = temps / 1000L;
        return String.format(TIME_SECOND, totalSecs % 60L);
    }

    public static String getStringTime(long second) {
        if (second < 60) return (TimerBuilder.getFormatLongseconds(second * 1000L));
        else if (second < 3600) return (TimerBuilder.getFormatLongMinutes(second * 1000L));
        else if (second < 86400) return (TimerBuilder.getFormatLongHours(second * 1000L));
        else return (TimerBuilder.getFormatLongDays(second * 1000L));
    }
}
