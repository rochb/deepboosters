package me.sharkz.deepbooster.core.utils;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.core.inventories.InventoryManager;
import me.sharkz.deepbooster.core.utils.builders.CooldownBuilder;
import me.sharkz.deepbooster.core.utils.builders.TimerBuilder;
import me.sharkz.deepbooster.core.utils.mc.ActionBar;
import me.sharkz.deepbooster.core.utils.mc.Cuboid;
import me.sharkz.deepbooster.core.utils.particles.ParticleHandler;
import me.sharkz.deepbooster.core.utils.particles.ParticleType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Constructor;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LUtils {

    private static final transient List<String> teleportPlayers = new ArrayList<>();
    protected transient JavaPlugin plugin = DeepBooster.getInstance();


    protected void spawnParticle(Player player, Location location, ParticleType type) {
        ParticleHandler.spawnParticleForPlayers(location, type, player);
    }

    protected void spawnParticle(Location location, ParticleType type) {
        Bukkit.getOnlinePlayers().forEach(player -> spawnParticle(player, location, type));
    }

    protected Location changeStringLocationToLocation(String s) {
        String[] a = s.split(",");
        if (a.length == 6) return changeStringLocationToLocationEye(s);
        World w = Bukkit.getServer().getWorld(a[0]);
        float x = Float.parseFloat(a[1]);
        float y = Float.parseFloat(a[2]);
        float z = Float.parseFloat(a[3]);
        return new Location(w, x, y, z);
    }


    protected Location changeStringLocationToLocationEye(String s) {
        String[] a = s.split(",");
        World w = Bukkit.getServer().getWorld(a[0]);
        float x = Float.parseFloat(a[1]);
        float y = Float.parseFloat(a[2]);
        float z = Float.parseFloat(a[3]);
        float yaw = Float.parseFloat(a[4]);
        float pitch = Float.parseFloat(a[5]);
        return new Location(w, x, y, z, yaw, pitch);
    }


    protected String changeLocationToString(Location location) {
        return location.getWorld().getName() + "," + location.getX() + "," + location.getY() + ","
                + location.getZ();
    }


    protected String changeLocationToStringEye(Location location) {
        return location.getWorld().getName() + "," + location.getX() + "," + location.getY() + ","
                + location.getZ() + "," + location.getYaw() + "," + location.getPitch();
    }


    protected Chunk changeStringChuncToChunk(String chunk) {
        String[] a = chunk.split(",");
        World w = Bukkit.getServer().getWorld(a[0]);
        return w.getChunkAt(Integer.valueOf(a[1]), Integer.valueOf(a[2]));
    }


    protected String changeChunkToString(Chunk chunk) {
        return chunk.getWorld().getName() + "," + chunk.getX() + "," + chunk.getZ();
    }


    protected String changeCuboidToString(Cuboid cuboid) {
        return cuboid.getWorld().getName() + "," + cuboid.getLowerX() + "," + cuboid.getLowerY() + ","
                + cuboid.getLowerZ() + "," + ";" + cuboid.getWorld().getName() + "," + cuboid.getUpperX() + ","
                + cuboid.getUpperY() + "," + cuboid.getUpperZ();
    }


    protected Cuboid changeStringToCuboid(String str) {

        String[] parsedCuboid = str.split(";");
        String[] parsedFirstLoc = parsedCuboid[0].split(",");
        String[] parsedSecondLoc = parsedCuboid[1].split(",");

        String firstWorldName = parsedFirstLoc[0];
        double firstX = Double.parseDouble(parsedFirstLoc[1]);
        double firstY = Double.parseDouble(parsedFirstLoc[2]);
        double firstZ = Double.parseDouble(parsedFirstLoc[3]);

        String secondWorldName = parsedSecondLoc[0];
        double secondX = Double.parseDouble(parsedSecondLoc[1]);
        double secondY = Double.parseDouble(parsedSecondLoc[2]);
        double secondZ = Double.parseDouble(parsedSecondLoc[3]);

        Location l1 = new Location(Bukkit.getWorld(firstWorldName), firstX, firstY, firstZ);

        Location l2 = new Location(Bukkit.getWorld(secondWorldName), secondX, secondY, secondZ);

        return new Cuboid(l1, l2);

    }


    protected String betterMaterial(Material material) {
        return TextUtil.getMaterialLowerAndMajAndSpace(material);
    }


    protected int getNumberBetween(int a, int b) {
        return ThreadLocalRandom.current().nextInt(a, b);
    }


    protected boolean hasInventoryFull(Player player) {
        int slot = 0;
        ItemStack[] arrayOfItemStack;
        int x = (arrayOfItemStack = player.getInventory().getContents()).length;
        for (int i = 0; i < x; i++) {
            ItemStack contents = arrayOfItemStack[i];
            if ((contents == null))
                slot++;
        }
        return slot == 0;
    }

    protected boolean give(ItemStack item, Player player) {
        if (hasInventoryFull(player))
            return false;
        player.getInventory().addItem(item);
        return true;
    }


    protected void give(Player player, ItemStack item) {
        if (hasInventoryFull(player))
            player.getWorld().dropItem(player.getLocation(), item);
        else
            player.getInventory().addItem(item);
    }

    protected boolean same(ItemStack stack, String name) {
        return stack.hasItemMeta() && stack.getItemMeta().hasDisplayName()
                && stack.getItemMeta().getDisplayName().equals(name);
    }


    protected boolean contains(ItemStack stack, String name) {
        return stack.hasItemMeta() && stack.getItemMeta().hasDisplayName()
                && stack.getItemMeta().getDisplayName().contains(name);
    }


    protected void removeItemInHand(Player player) {
        removeItemInHand(player, 64);
    }


    protected void removeItemInHand(Player player, int how) {
        if (player.getInventory().getItemInMainHand().getAmount() > how)
            player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
        else player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
        player.updateInventory();
    }


    protected boolean same(Location l, Location l2) {
        return (l.getBlockX() == l2.getBlockX()) && (l.getBlockY() == l2.getBlockY())
                && (l.getBlockZ() == l2.getBlockZ()) && l.getWorld().getName().equals(l2.getWorld().getName());
    }

    protected void sphereParticle(Location location, ParticleType type) {
        getSphere(location).forEach(loc -> spawnParticle(loc, type));
    }

    protected void circleParticle(Location location, ParticleType type, int radius, int amount) {
        getCircle(location, radius, amount).forEach(loc -> spawnParticle(loc, type));
    }


    protected List<Location> getSphere(Location center) {
        List<Location> locations = new ArrayList<>();
        double p = 0;
        while(p > 2){
            p += Math.PI / 10;
            for (double t = 0; t <= 360; t += Math.PI / 40) {
                double r = 1.5;
                double x = r * Math.cos(t) * Math.sin(p);
                double y = r * Math.cos(p) + 1.5;
                double z = r * Math.sin(t) * Math.sin(p);
                locations.add(new Location(center.getWorld(), x, y, z));
            }
        }
        return locations;
    }

    protected List<Location> getCircle(Location center, double radius, int amount) {
        World world = center.getWorld();
        double increment = (2 * Math.PI) / amount;
        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            double angle = i * increment;
            double x = center.getX() + (radius * Math.cos(angle));
            double z = center.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(world, x, center.getY(), z));
        }
        return locations;
    }

    protected String format(double decimal) {
        return format(decimal, "#.##");
    }


    protected String format(double decimal, String format) {
        DecimalFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(decimal);
    }


    protected void removeItems(Player player, int item, ItemStack itemStack) {
        for (ItemStack is : player.getInventory().getContents()) {
            if (is != null && is.isSimilar(itemStack)) {
                int currentAmount = is.getAmount() - item;
                item -= is.getAmount();
                if (currentAmount <= 0)
                    player.getInventory().removeItem(is);
                else
                    is.setAmount(currentAmount);
            }
        }
        player.updateInventory();
    }


    protected void schedule(long delay, Runnable runnable) {
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                if (runnable != null)
                    runnable.run();
            }
        }, delay);
    }


    protected String name(String string) {
        return TextUtil.name(string);
    }


    protected int getMaxPage(Collection<?> items) {
        if (items == null || items.size() < 1) return 0;
        return (items.size() / 45) + 1;
    }


    protected int getMaxPage(Collection<?> items, int a) {
        if (items == null || items.size() < 1) return 0;
        return (items.size() / a) + 1;
    }


    protected double percent(double value, double total) {
        return (value * 100) / total;
    }


    protected double percentNum(double total, double percent) {
        return total * (percent / 100);
    }


    protected void schedule(long delay, int count, Runnable runnable) {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            int tmpCount = 0;

            @Override
            public void run() {

                if (!plugin.isEnabled()) {
                    cancel();
                    return;
                }

                if (tmpCount > count) {
                    cancel();
                    return;
                }

                tmpCount++;
                Bukkit.getScheduler().runTask(plugin, runnable);

            }
        }, 0, delay);
    }


    protected void message(CommandSender player, String message) {
        player.sendMessage(DeepBooster.PREFIX + " " + color(message));
    }


    protected void messageWO(CommandSender player, String msg) {
        player.sendMessage(color(msg));
    }


    protected void messageWO(CommandSender player, String msg, Object... args) {
        player.sendMessage(String.format(color(msg), args));
    }


    protected void message(CommandSender player, String msg, Object... args) {
        player.sendMessage(DeepBooster.PREFIX + " " + String.format(color(msg), args));
    }


    protected void actionMessage(Player player, String msg, Object... args) {
        ActionBar.sendActionBar(player, String.format(color(msg), args));
    }

    protected void titleMessage(Player player, String msg) {
        sendTitle(player, color(msg), "", 1, 3, 1);
    }

    protected void titleMessage(Player player, String line, Object... args) {
        sendTitle(player, String.format(color(line), args), "", 1, 3, 1);
    }

    protected void titleMessage(Player player, String line1, String line2, Object... args) {
        sendTitle(player, String.format(color(line1), args), color(line2), 1, 3, 1);
    }

    protected void titleMessage(Player player, String line1, String line2) {
        sendTitle(player, color(line1), color(line2), 1, 3, 1);
    }

    protected boolean hasPermission(Permissible permissible, String permission) {
        return permissible.hasPermission(permission);
    }

    protected void scheduleFix(long delay, BiConsumer<TimerTask, Boolean> runnable) {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!plugin.isEnabled()) {
                    cancel();
                    runnable.accept(this, false);
                    return;
                }
                Bukkit.getScheduler().runTask(plugin, () -> runnable.accept(this, true));
            }
        }, delay, delay);
    }

    protected void runTaskLater(long delay, BiConsumer<TimerTask, Boolean> runnable) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (!plugin.isEnabled()) {
                    cancel();
                    runnable.accept(this, false);
                    return;
                }
                Bukkit.getScheduler().runTask(plugin, () -> runnable.accept(this, true));
            }
        }, delay);
    }


    protected <T> T randomElement(List<T> element) {
        if (element.size() == 0)
            return null;
        if (element.size() == 1)
            return element.get(0);
        Random random = new Random();
        return element.get(random.nextInt(element.size() - 1));
    }


    protected String getItemName(ItemStack item) {
        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
            return item.getItemMeta().getDisplayName();
        String name = item.serialize().get("type").toString().replace("_", " ").toLowerCase();
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }


    protected String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }


    public String colorReverse(String message) {
        return ChatColor.stripColor(message);
    }


    protected List<String> color(List<String> messages) {
        return messages.stream().map(this::color).collect(Collectors.toList());
    }


    public List<String> colorReverse(List<String> messages) {
        return messages.stream().map(this::colorReverse).collect(Collectors.toList());
    }


    protected ItemFlag getFlag(String flagString) {
        for (ItemFlag flag : ItemFlag.values()) {
            if (flag.name().equalsIgnoreCase(flagString))
                return flag;
        }
        return null;
    }


    protected <T> List<T> reverse(List<T> list) {
        List<T> tmpList = new ArrayList<>();
        for (int index = list.size() - 1; index != -1; index--)
            tmpList.add(list.get(index));
        return tmpList;
    }


    protected String price(long price) {
        return String.format("%,d", price);
    }


    protected String generateRandomString(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }


    protected TextComponent buildTextComponent(String message) {
        return new TextComponent(message);
    }


    protected TextComponent setHoverMessage(TextComponent component, String... messages) {
        BaseComponent[] list = new BaseComponent[messages.length];
        for (int a = 0; a != messages.length; a++)
            list[a] = new TextComponent(messages[a] + (messages.length - 1 == a ? "" : "\n"));
        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, list));
        return component;
    }


    protected TextComponent setHoverMessage(TextComponent component, List<String> messages) {
        BaseComponent[] list = new BaseComponent[messages.size()];
        for (int a = 0; a != messages.size(); a++)
            list[a] = new TextComponent(messages.get(a) + (messages.size() - 1 == a ? "" : "\n"));
        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, list));
        return component;
    }


    protected TextComponent setClickAction(TextComponent component, ClickEvent.Action action, String command) {
        component.setClickEvent(new ClickEvent(action, command));
        return component;
    }


    protected void removeItems(Inventory inventory, ItemStack removeItemStack, int amount) {
        for (ItemStack itemStack : inventory.getContents()) {
            if (itemStack != null && itemStack.isSimilar(itemStack) && amount > 0) {
                int currentAmount = itemStack.getAmount() - amount;
                amount -= itemStack.getAmount();
                if (currentAmount <= 0) inventory.removeItem(itemStack);
                else itemStack.setAmount(currentAmount);
            }
        }
    }


    protected String getDisplayBalence(double value) {
        if (value < 10000)
            return format(value, "#.#");
        else if (value < 1000000)
            return (int) (value / 1000) + "k ";
        else if (value < 1000000000)
            return format((value / 1000) / 1000, "#.#") + "m ";
        else if (value < 1000000000000L)
            return (int) (((value / 1000) / 1000) / 1000) + "M ";
        else
            return "too much";
    }


    protected String getDisplayBalence(long value) {
        if (value < 10000)
            return format(value, "#.#");
        else if (value < 1000000)
            return (int) (value / 1000) + "k ";
        else if (value < 1000000000)
            return format((value / 1000) / 1000, "#.#") + "m ";
        else if (value < 1000000000000L)
            return (int) (((value / 1000) / 1000) / 1000) + "M ";
        else
            return "too much";
    }


    protected int count(Inventory inventory, Material material) {
        int count = 0;
        for (ItemStack itemStack : inventory.getContents())
            if (itemStack != null && itemStack.getType().equals(material))
                count += itemStack.getAmount();
        return count;
    }


    protected Enchantment enchantFromString(String str) {
        for (Enchantment enchantment : Enchantment.values())
            if (enchantment.getName().equalsIgnoreCase(str))
                return enchantment;
        return null;
    }


    protected BlockFace getClosestFace(float direction) {

        direction = direction % 360;

        if (direction < 0)
            direction += 360;

        direction = Math.round(direction / 45);

        switch ((int) direction) {
            case 0:
                return BlockFace.WEST;
            case 1:
                return BlockFace.NORTH_WEST;
            case 2:
                return BlockFace.NORTH;
            case 3:
                return BlockFace.NORTH_EAST;
            case 4:
                return BlockFace.EAST;
            case 5:
                return BlockFace.SOUTH_EAST;
            case 6:
                return BlockFace.SOUTH;
            case 7:
                return BlockFace.SOUTH_WEST;
            default:
                return BlockFace.WEST;
        }
    }


    protected String betterPrice(long price) {
        String betterPrice = "";
        String[] splitPrice = String.valueOf(price).split("");
        int current = 0;
        for (int a = splitPrice.length - 1; a > -1; a--) {
            current++;
            if (current > 3) {
                betterPrice += ".";
                current = 1;
            }
            betterPrice += splitPrice[a];
        }
        StringBuilder builder = new StringBuilder().append(betterPrice);
        builder.reverse();
        return builder.toString();
    }


    protected boolean hasEnchant(Enchantment enchantment, ItemStack itemStack) {
        return itemStack.hasItemMeta() && itemStack.getItemMeta().hasEnchants()
                && itemStack.getItemMeta().hasEnchant(enchantment);
    }


    protected String timerFormat(Player player, String cooldown) {
        return TimerBuilder.getStringTime(CooldownBuilder.getCooldownPlayer(cooldown, player) / 1000);
    }


    protected boolean isCooldown(Player player, String cooldown) {
        return isCooldown(player, cooldown, 0);
    }


    protected boolean isCooldown(Player player, String cooldown, int timer) {
        if (CooldownBuilder.isCooldown(cooldown, player)) return true;
        if (timer != 0) CooldownBuilder.addCooldown(cooldown, player, timer);
        return false;
    }

    protected void addCooldown(Player player, String cooldown, int time) {
        CooldownBuilder.createCooldown(cooldown);
        CooldownBuilder.addCooldown(cooldown, player, time);
    }


    protected void createInventory(InventoryManager manager, Player player, int inventoryId) {
        createInventory(manager, player, inventoryId, 1);
    }

    /**
     * @param player
     * @param inventoryId
     * @param page
     */
    protected void createInventory(InventoryManager manager, Player player, int inventoryId, int page) {
        createInventory(manager, player, inventoryId, page, new Object() {
        });
    }

    /**
     * @param player
     * @param inventoryId
     * @param page
     * @param objects
     */
    protected void createInventory(InventoryManager manager, Player player, int inventoryId, int page, Object... objects) {
        manager.createInventory(inventoryId, player, page, objects);
    }

    protected String toList(Stream<String> list) {
        return toList(list.collect(Collectors.toList()), "&e", "&6");
    }


    protected String toList(List<String> list) {
        return toList(list, "&e", "&6&n");
    }


    protected String toList(List<String> list, String color, String color2) {
        if (list == null || list.size() == 0) return null;
        if (list.size() == 1) return list.get(0);
        String str = "";
        for (int a = 0; a != list.size(); a++) {
            if (a == list.size() - 1 && a != 0) str += color + " and " + color2;
            else if (a != 0) str += color + ", " + color2;
            str += list.get(a);
        }
        return str;
    }

    protected void centeredMessage(CommandSender player, String message) {
        player.sendMessage(TextUtil.centeredText(message));
    }

    protected void centeredMessage(CommandSender player, TextComponent message) {
        player.spigot().sendMessage(TextUtil.centeredText(message));
    }

    protected void centeredMessage(CommandSender player, BaseComponent message) {
        player.spigot().sendMessage(TextUtil.centeredText(new TextComponent(message)));
    }

    public int getNearestMultiple(int i, int number) {
        return ((i + (number - 1)) / number) * number;
    }


    protected void message(CommandSender player, TextComponent message) {
        player.spigot().sendMessage(message);
    }


    protected void message(CommandSender player, BaseComponent message) {
        player.spigot().sendMessage(message);
    }


    protected boolean isOutsideBorder(Location loc) {
        WorldBorder border = loc.getWorld().getWorldBorder();
        double radius = border.getSize() / 2;
        Location center = border.getCenter();
        double maxX = center.getX() + radius;
        double minX = center.getX() - radius;
        double minZ = center.getZ() - radius;
        double maxZ = center.getZ() + radius;

        return loc.getX() < minX || loc.getX() > maxX || loc.getZ() < minZ || loc.getZ() > maxZ;
    }

    protected boolean isNearBorder(Location loc, int range) {
        WorldBorder border = loc.getWorld().getWorldBorder();
        double radius = (border.getSize() / 2) - range;
        Location center = border.getCenter();
        double maxX = center.getX() + radius;
        double minX = center.getX() - radius;
        double minZ = center.getZ() - radius;
        double maxZ = center.getZ() + radius;

        return loc.getX() < minX || loc.getX() > maxX || loc.getZ() < minZ || loc.getZ() > maxZ;
    }

    protected Inventory setInventoryContents(Inventory inv, Player targetPlayer) {
        ItemStack[] items = targetPlayer.getInventory().getContents();
        ItemStack[] armor = targetPlayer.getInventory().getArmorContents();
        reverse(armor);

        int i;
        for (i = 0; i < items.length; ++i)
            inv.setItem(i, items[i]);

        for (i = 0; i <= armor.length - 1; ++i) {
            if (i == 3)
                inv.setItem(39 + i, targetPlayer.getItemInHand());
            inv.setItem(38 + i, armor[i]);
        }

        return inv;
    }

    protected Map<Integer, ItemStack> getInventoryContents(Player p) {
        Map<Integer, ItemStack> inv = new HashMap<>();
        ItemStack[] items = p.getInventory().getContents();
        ItemStack[] armor = p.getInventory().getArmorContents();
        reverse(armor);

        int i;
        for (i = 0; i < items.length; ++i)
            inv.put(i, items[i]);

        for (i = 0; i <= armor.length - 1; ++i) {
            if (i == 3)
                inv.put(49 + i, p.getItemInHand());
            inv.put(38 + i, armor[i]);
        }
        return inv;
    }

    protected void reverse(Object[] array) {
        if (array != null) {
            int i = 0;

            for (int j = Math.min(array.length, array.length) - 1; j > i; ++i) {
                Object tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
                --j;
            }
        }
    }

    protected String serializeLocation(Location location) {
        return location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();
    }

    protected boolean isDamaged(ItemStack i) {
        return i.getDurability() > (short) 0;
    }

    protected void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server."
                    + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void sendTitle(Player player, String title, String subtitle, int fadeInTime, int showTime, int fadeOutTime) {
        try {
            Object chatTitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class)
                    .invoke(null, "{\"text\": \"" + title + "\"}");
            Constructor<?> titleConstructor = getNMSClass("PacketPlayOutTitle").getConstructor(
                    getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], getNMSClass("IChatBaseComponent"),
                    int.class, int.class, int.class);
            Object packet = titleConstructor.newInstance(
                    getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null), chatTitle,
                    fadeInTime, showTime, fadeOutTime);

            Object chatsTitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class)
                    .invoke(null, "{\"text\": \"" + subtitle + "\"}");
            Constructor<?> timingTitleConstructor = getNMSClass("PacketPlayOutTitle").getConstructor(
                    getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], getNMSClass("IChatBaseComponent"),
                    int.class, int.class, int.class);
            Object timingPacket = timingTitleConstructor.newInstance(
                    getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null), chatsTitle,
                    fadeInTime, showTime, fadeOutTime);

            sendPacket(player, packet);
            sendPacket(player, timingPacket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void pushBack(Player player, int force, Location location) {
        Location t = player.getLocation().subtract(location);
        double distance = player.getLocation().distance(location);
        t.getDirection().normalize().multiply(-force);
        t.multiply(distance / (0.1 * force));
        player.setVelocity(t.toVector());
    }

    protected int random(int min, int max) {
        if (min >= max) throw new IllegalArgumentException("max must be greater than min");
        return new Random().nextInt((max - min) + 1) + min;
    }
}
