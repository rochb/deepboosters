package me.sharkz.deepbooster.core.utils.particles;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;

/**
 * Class to handle particle effects using reflection. Hopefully version
 * independent.
 */
public final class ParticleHandler {

    private static Class<?> particleClass;
    private static Class<?> particleEnum;
    private static Class<?>[] parameters;

    private static boolean useNewParticleSystem = false;

    static {
        try {
            particleClass = PacketHandler.getNMSClass("PacketPlayOutWorldParticles");
            particleEnum = PacketHandler.getNMSClass("EnumParticle");

            parameters = new Class<?>[] {particleEnum, boolean.class, float.class, float.class, float.class,
                    float.class, float.class, float.class, float.class, int.class, int[].class};
        } catch (ClassNotFoundException ignored) {
            useNewParticleSystem = true;
        }
    }

    /**
     * Spawns (particle) at (loc), visible to all of (forPlayer)
     *
     * @param loc      The location at which to display the particle.
     * @param type The particle to display.
     * @param players  The player(s) for whom to display the particles.
     */
    public static void spawnParticleForPlayers(Location loc, ParticleType type, Player... players) {
        try {
            spawnParticle(loc, players, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void spawnParticle(Location loc, Player[] players, ParticleType type) throws Exception {
        if (useNewParticleSystem) spawn(loc, players, type);
        else spawnLegacy(loc, players, type);
    }

    private static void spawn(Location loc, Player[] players, ParticleType type) {
        final Particle bukkitParticle = Particle.valueOf(type.name());
        //noinspection ConstantConditions
        if (bukkitParticle == null)
            return;
        for (Player p : players) p.spawnParticle(bukkitParticle, loc, 1, 0.0d, 0.0d, 0.0d, 0.0d, null);
    }

    private static void spawnLegacy(Location loc, Player[] players, ParticleType type) throws Exception {
        if (particleClass == null || particleEnum == null || parameters == null)
            return;
        Constructor<?> constructor = particleClass.getConstructor(parameters);
        //noinspection JavaReflectionInvocation
        Object packet = constructor.newInstance(particleEnum.getField(type.name()).get(null), true, (float) loc.getX(),
                (float) loc.getY(), (float) loc.getZ(), 0f, 0f, 0f, 1f, 0, new int[0]);
        for (Player p : players) PacketHandler.sendPacket(p, packet);
    }
}
