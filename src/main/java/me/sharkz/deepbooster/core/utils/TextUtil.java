package me.sharkz.deepbooster.core.utils;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class TextUtil {

	/*
	 * Material name
	 */

	public static String getMaterialLower(Material material) {
		return material.name().toLowerCase();
	}

	public static String getMaterialLowerAndSpace(Material material) {
		return material.name().replace("_", " ").toLowerCase();
	}

	public static String getMaterialLowerAndMaj(Material material) {
		return material.name().toLowerCase().replace(material.name().toCharArray()[0],
				Character.toUpperCase(material.name().toCharArray()[0]));
	}

	public static String getMaterialLowerAndMajAndSpace(Material material) {
		return material.name().replace("_", " ").toLowerCase().replace(material.name().toCharArray()[0],
				Character.toUpperCase(material.name().toCharArray()[0]));
	}


	public static String name(String m) {
		String name =  m.replace("_", " ").toLowerCase();
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}
	
	/*
	 * Parse text
	 */

	public static String parse(String str, Object... args) {
		return String.format(str, args);
	}

	/*
	 * Parce color
	 */

	public static String color(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	public static List<String> color(List<String> messages) {
		List<String> newList = new ArrayList<>();
		messages.forEach(message -> newList.add(color(message)));
		return newList;
	}

	public static String m(String message, Object... obj) {
		return String.format(message, obj);
	}

	public static String centeredText(String message){
		message = color(message);
		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;
		int charIndex = 0;
		int lastSpaceIndex = 0;
		for(char c : message.toCharArray()){
			if(c == '§'){
				previousCode = true;
				continue;
			}else if(previousCode){
				previousCode = false;
				if(c == 'l' || c == 'L'){
					isBold = true;
					continue;
				}else isBold = false;
			}else if(c == ' ') lastSpaceIndex = charIndex;
			else{
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
			}
			if(messagePxSize >= 250){
				message = message.substring(0, lastSpaceIndex + 1);
				break;
			}
			charIndex++;
		}
		int halvedMessageSize = messagePxSize / 2;
		int toCompensate = 154 - halvedMessageSize;
		int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
		int compensated = 0;
		StringBuilder sb = new StringBuilder();
		while(compensated < toCompensate){
			sb.append(" ");
			compensated += spaceLength;
		}
		return sb.toString() + message;
	}

	public static TextComponent centeredText(TextComponent message){
		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;
		int charIndex = 0;
		int lastSpaceIndex = 0;
		for(char c : message.getText().toCharArray()){
			if(c == '§'){
				previousCode = true;
				continue;
			}else if(previousCode){
				previousCode = false;
				if(c == 'l' || c == 'L'){
					isBold = true;
					continue;
				}else isBold = false;
			}else if(c == ' ') lastSpaceIndex = charIndex;
			else{
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
			}
			charIndex++;
		}
		int halvedMessageSize = messagePxSize / 2;
		int toCompensate = 154 - halvedMessageSize;
		int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
		int compensated = 0;
		StringBuilder sb = new StringBuilder();
		while(compensated < toCompensate){
			sb.append(" ");
			compensated += spaceLength;
		}
		TextComponent c = new TextComponent(TextComponent.fromLegacyText(sb.toString()));
		c.addExtra(message);
		return c;
	}
}
