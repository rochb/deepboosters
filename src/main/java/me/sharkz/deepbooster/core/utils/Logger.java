package me.sharkz.deepbooster.core.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {

    private final String prefix;
    private static Logger logger;

    public Logger(String prefix) {
        this.prefix = prefix;
        logger = this;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void info(String message, LogType type){
        getLogger().log(message, type);
    }

    public static void info(String message){
        getLogger().log(message, LogType.INFO);
    }

    public String getPrefix() {
        return prefix;
    }

    public enum LogType{
        ERROR("&c"),
        INFO("&7"),
        WARNING("&e"),
        SUCCESS("&a");

        private final String color;

        private LogType(String color) {
            this.color = color;
        }

        public String getColor() {
            return ChatColor.translateAlternateColorCodes('&', color);
        }
    }

    public void log(String message, LogType type){
        Bukkit.getConsoleSender().sendMessage(getColoredMessage("&9"+prefix+"&7 | ") + type.getColor() + getColoredMessage(message));
    }

    public void log(String message){
        Bukkit.getConsoleSender().sendMessage(getColoredMessage("&9"+prefix+"&7 | &e" + message));
    }

    public void log(String message, Object... args){
        log(String.format(message, args));
    }

    public void log(String message,  LogType type, Object... args){
        log(String.format(message, args), type);
    }

    public void log(String[] messages, LogType type){
        for(String message : messages){
            log(message, type);
        }
    }

    public String getColoredMessage(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
