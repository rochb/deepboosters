package me.sharkz.deepbooster.core.utils.builders;

import me.sharkz.deepbooster.core.utils.LUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;

public class ItemBuilder extends LUtils implements Cloneable {

	private ItemStack item;
	private final Material material;
	private ItemMeta meta;
	private int data;
	private int amount;
	private String name;
	private List<String> lore;
	private List<ItemFlag> flags;
	private int durability;
	private Map<Enchantment, Integer> enchantments;
	private Map<Enchantment, Integer> unsafe_enchant;

	public ItemBuilder(ItemStack i){
		this.material = i.getType();
		this.data = i.getData().getData();
		this.amount = i.getAmount();
		this.name = i.getItemMeta().getDisplayName();
		this.lore = i.getItemMeta().getLore();
		this.flags = new ArrayList<>(i.getItemMeta().getItemFlags());
		this.unsafe_enchant = i.getEnchantments();
		this.meta = i.getItemMeta();
	}

	public ItemBuilder(ItemStack i, int amount){
		this.material = i.getType();
		this.data = i.getData().getData();
		this.amount = amount;
		this.name = i.getItemMeta().getDisplayName();
		this.lore = i.getItemMeta().getLore();
		this.flags = new ArrayList<>(i.getItemMeta().getItemFlags());
		this.unsafe_enchant = i.getEnchantments();
		this.meta = i.getItemMeta();
	}


	public ItemBuilder(String base64){
		this(SkullBuilder.itemFromBase64(base64));
	}

	public ItemBuilder(UUID uuid){
		this(SkullBuilder.itemFromUuid(uuid));
	}

	public ItemBuilder(Material material, int data, int amount, String name, List<String> lore, List<ItemFlag> flags,
			Map<Enchantment, Integer> enchantments) {
		super();
		this.material = material;
		this.data = data;
		this.amount = amount;
		this.name = name;
		this.lore = lore;
		this.flags = flags;
		this.enchantments = enchantments;
	}

	public ItemBuilder(Material material) {
		this(material, 1);
	}

	public ItemBuilder(Material material, int amount) {
		this(material, 0, amount);
	}

	public ItemBuilder(Material material, int amount, int data) {
		this(material, data, amount, null);
	}

	public ItemBuilder(Material material, int amount, int data, String name) {
		this(material, data, amount, name, null, null, null);
	}

	public ItemBuilder(Material material, int amount, String name) {
		this(material, 0, amount, name, null, null, null);
	}

	public ItemBuilder(Material material, String name) {
		this(material, 0, 1, name, null, null, null);
	}

	public ItemBuilder(Material material, ItemFlag... flags) {
		this(material, 0, 1, null, null, Arrays.asList(flags), null);
	}

	public ItemBuilder(Material material, String... lore) {
		this(material, 0, 1, null, Arrays.asList(lore), null, null);
	}

	public ItemBuilder addEnchant(Enchantment enchantment, int value) {
		if (enchantments == null)
			enchantments = new HashMap<Enchantment, Integer>();
		enchantments.put(enchantment, value);
		return this;
	}

	public ItemBuilder addUnsafeEnchant(Enchantment enchantment, int value) {
		if (unsafe_enchant == null) unsafe_enchant = new HashMap<>();
		unsafe_enchant.put(enchantment, value);
		return this;
	}


	public ItemBuilder setFlag(ItemFlag... flags) {
		this.flags = Arrays.asList(flags);
		return this;
	}

	public ItemBuilder setFlag(ItemFlag flag) {
		if (flags == null) flags = new ArrayList<>();
		this.flags.add(flag);
		return this;
	}

	/**
	 * 
	 * @param format
	 * @param args
	 * @return
	 */
	public ItemBuilder addLine(String format, Object... args) {
		if (lore == null) lore = new ArrayList<>();
		lore.add(String.format(format, args));
		return this;
	}

	public ItemBuilder addLore(String line){
		if(lore == null) lore = new ArrayList<>();
		lore.add(line);
		return this;
	}

	public ItemBuilder addLore(String...lores){
		List<String> listFinal = new ArrayList<>();
		if(lore == null) listFinal = new ArrayList<>(Arrays.asList(lores));
		else {
			listFinal.addAll(lore);
			listFinal.addAll(Arrays.asList(lores));
		}
		lore = listFinal;
		return this;
	}

	public ItemBuilder setLore(String... lores) {
		this.lore = Arrays.asList(lores);
		return this;
	}

	public ItemBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public ItemBuilder durability(int durability) {
		this.durability = durability;
		return this;
	}

	public ItemBuilder glow() {
		addUnsafeEnchant(Enchantment.DURABILITY, 1);
		setFlag(ItemFlag.HIDE_ENCHANTS);
		return this;
	}

	public ItemBuilder setAmount(int amount){
		this.amount = amount;
		return this;
	}

	public ItemStack build() {
		item = new ItemStack(material, amount, (short) data);
		if (meta == null) meta = item.getItemMeta();
		if (flags != null && flags.size() > 0) flags.forEach(flag -> meta.addItemFlags(flag));
		if (name != null) meta.setDisplayName(color(name));
		if (lore != null) meta.setLore(color(lore));
		item.setItemMeta(meta);
		if(enchantments != null) enchantments.forEach((enchant, value) -> item.addEnchantment(enchant, value));
		if(unsafe_enchant != null) unsafe_enchant.forEach((enchant, value) -> item.addUnsafeEnchantment(enchant, value));
		return item;
	}

	public ItemBuilder clone() {
		try {
			return (ItemBuilder) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return the item
	 */
	public ItemStack getItem() {
		return item;
	}

	/**
	 * @return the material
	 */
	public Material getMaterial() {
		return material;
	}

	/**
	 * @return the meta
	 */
	public ItemMeta getMeta() {
		return meta;
	}

	/**
	 * @return the data
	 */
	public int getData() {
		return data;
	}

	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the lore
	 */
	public List<String> getLore() {
		return lore;
	}

	/**
	 * @return the flags
	 */
	public List<ItemFlag> getFlags() {
		return flags;
	}

	/**
	 * @return the durability
	 */
	public int getDurability() {
		return durability;
	}

	/**
	 * @return the enchantments
	 */
	public Map<Enchantment, Integer> getEnchantments() {
		return enchantments;
	}

}
