package me.sharkz.deepbooster.core.exceptions;

public class ItemAlreadyExistException extends Error {

    public ItemAlreadyExistException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ItemAlreadyExistException(String message, Throwable cause, boolean enableSuppression,
                                          boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public ItemAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public ItemAlreadyExistException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ItemAlreadyExistException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     *
     */
    private static final long serialVersionUID = -5611455794293459581L;

}
