package me.sharkz.deepbooster.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class ListenersManager {

    private List<ListenerAdapter> listeners = new ArrayList<>();
    private final JavaPlugin plugin;

    public ListenersManager(JavaPlugin plugin) {
        this.plugin = plugin;
        register(new AdapterListener(this));
    }

    public void register(ListenerAdapter adapter){
        listeners.add(adapter);
    }

    public void register(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, plugin);
    }

    public List<ListenerAdapter> getListenerAdapters() {
        return listeners;
    }
}
