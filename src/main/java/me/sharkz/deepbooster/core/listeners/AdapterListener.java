package me.sharkz.deepbooster.core.listeners;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import me.sharkz.deepbooster.core.utils.LUtils;
import net.brcdev.shopgui.event.ShopPreTransactionEvent;
import net.brcdev.shopgui.shop.ShopManager;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;

public class AdapterListener extends LUtils implements Listener {

    private final ListenersManager manager;

    public AdapterListener(ListenersManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onConnect(PlayerJoinEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onConnect(event, event.getPlayer()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onQuit(event, event.getPlayer()));
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onMove(event, event.getPlayer()));
        if (event.getFrom().getBlockX() >> 1 == event.getTo().getBlockX() >> 1
                && event.getFrom().getBlockZ() >> 1 == event.getTo().getBlockZ() >> 1
                && event.getFrom().getWorld() == event.getTo().getWorld())
            return;
        manager.getListenerAdapters().forEach(adapter -> adapter.onPlayerWalk(event, event.getPlayer(), 1));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        manager.getListenerAdapters()
                .forEach(adapter -> adapter.onInventoryClick(event, (Player) event.getWhoClicked()));
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onBlockBreak(event, event.getPlayer()));
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onBlockPlace(event, event.getPlayer()));
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onEntityDeath(event, event.getEntity()));
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onInteract(event, event.getPlayer()));
    }

    @EventHandler
    public void onPlayerTalk(AsyncPlayerChatEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onPlayerTalk(event, event.getMessage()));
    }

    @EventHandler
    public void onCraftItem(CraftItemEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onCraftItem(event));
    }

    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        manager.getListenerAdapters()
                .forEach(adapter -> adapter.onInventoryDrag(event, (Player) event.getWhoClicked()));
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onInventoryClose(event, (Player) event.getPlayer()));
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        manager.getListenerAdapters()
                .forEach(adapter -> adapter.onCommand(event, event.getPlayer(), event.getMessage()));
    }

    @EventHandler
    public void onGamemodeChange(PlayerGameModeChangeEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onGamemodeChange(event, event.getPlayer()));
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onDrop(event, event.getPlayer()));
    }

    @EventHandler
    public void onMobSpawn(CreatureSpawnEvent event) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onMobSpawn(event));
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof LivingEntity && event.getDamager() instanceof LivingEntity)
            manager.getListenerAdapters().forEach(adapter -> adapter.onDamageByEntity(event, event.getCause(),
                    event.getDamage(), (LivingEntity) event.getDamager(), (LivingEntity) event.getEntity()));
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player)
            manager.getListenerAdapters().forEach(adapter -> adapter.onPlayerDamagaByPlayer(event, event.getCause(),
                    event.getDamage(), (Player) event.getDamager(), (Player) event.getEntity()));
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Projectile)
            manager.getListenerAdapters().forEach(adapter -> adapter.onPlayerDamagaByArrow(event, event.getCause(),
                    event.getDamage(), (Projectile) event.getDamager(), (Player) event.getEntity()));
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        manager.getListenerAdapters().forEach(adapter -> adapter.onPlayerTeleport(e, e.getPlayer(), e.getTo()));
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPlayerDeath(e, e.getEntity()));
    }

    @EventHandler
    public void onPlayerConsume(PlayerItemConsumeEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPlayerConsume(e, e.getPlayer(), e.getItem()));
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> {
            listenerAdapter.onEntityDamage(e, e.getEntity());
            if (e.getEntity() instanceof Player)
                listenerAdapter.onPlayerDamage(e, (Player) e.getEntity());
        });
    }

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onSignChange(e, e.getPlayer(), (Sign) e.getBlock().getState()));
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onEntitySpawn(e, e.getEntity()));
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPrepareCraft(e));
    }

    @EventHandler
    public void onItemCraft(CraftItemEvent e) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onItemCraft(e));
    }

    @EventHandler
    public void onLogin(AsyncPlayerPreLoginEvent e){
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onLogin(e));
    }

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent event){
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onBedEnter(event, event.getPlayer(), event.getBed()));
    }

    @EventHandler
    public void onPlayerBedLeave(PlayerBedLeaveEvent event) {
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onBedLeave(event, event.getPlayer(), event.getBed()));
    }

    @EventHandler
    protected void onPlayerClick(PlayerInteractAtEntityEvent e){
        if(!e.getRightClicked().getType().equals(EntityType.PLAYER)) return;
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPlayerClick(e, e.getPlayer(), (Player) e.getRightClicked()));
    }

    @EventHandler
    protected void onPlayerXp(PlayerExpChangeEvent e){
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPlayerXp(e, e.getPlayer(), e.getAmount()));
    }

    @EventHandler
    protected void onPlayerXpMCMMO(McMMOPlayerXpGainEvent e){
        manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onPlayerXpMCMMO(e, e.getPlayer(), e.getRawXpGained()));
    }

    @EventHandler
    protected void onShopPreTransaction(ShopPreTransactionEvent e){
        if(e.getShopAction().equals(ShopManager.ShopAction.SELL) || e.getShopAction().equals(ShopManager.ShopAction.SELL_ALL))
            manager.getListenerAdapters().forEach(listenerAdapter -> listenerAdapter.onShopSell(e, e.getPlayer(), e.getAmount(), e.getPrice()));
    }
}
