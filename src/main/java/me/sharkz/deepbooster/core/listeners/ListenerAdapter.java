package me.sharkz.deepbooster.core.listeners;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import me.sharkz.deepbooster.core.utils.LUtils;
import net.brcdev.shopgui.event.ShopPreTransactionEvent;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

public abstract class ListenerAdapter extends LUtils {

	protected void onConnect(PlayerJoinEvent event, Player player) {
	}

	protected void onQuit(PlayerQuitEvent event, Player player) {
	}

	protected void onMove(PlayerMoveEvent event, Player player) {
	}

	protected void onInventoryClick(InventoryClickEvent event, Player player) {
	}

	protected void onInventoryClose(InventoryCloseEvent event, Player player) {
	}

	protected void onInventoryDrag(InventoryDragEvent event, Player player) {
	}

	protected void onBlockBreak(BlockBreakEvent event, Player player) {
	}

	protected void onBlockPlace(BlockPlaceEvent event, Player player) {
	}

	protected void onEntityDeath(EntityDeathEvent event, Entity entity) {
	}

	protected void onInteract(PlayerInteractEvent event, Player player) {
	}

	protected void onPlayerTalk(AsyncPlayerChatEvent event, String message) {
	}

	protected void onCraftItem(CraftItemEvent event) {
	}

	protected void onCommand(PlayerCommandPreprocessEvent event, Player player, String message) {
	}

	protected void onGamemodeChange(PlayerGameModeChangeEvent event, Player player) {
	}

	protected void onDrop(PlayerDropItemEvent event, Player player) {
	}

	protected void onMobSpawn(CreatureSpawnEvent event) {
	}
	
	protected void onDamageByEntity(EntityDamageByEntityEvent event, DamageCause cause, double damage, LivingEntity damager,
			LivingEntity entity) {
	}

	protected void onPlayerDamagaByPlayer(EntityDamageByEntityEvent event, DamageCause cause, double damage,
			Player damager, Player entity) {
	}

	protected void onPlayerDamagaByArrow(EntityDamageByEntityEvent event, DamageCause cause, double damage,
			Projectile damager, Player entity) {
	}

	protected void onItemisOnGround(PlayerDropItemEvent event, Player player, Item item, Location location) {
	}

	protected void onItemMove(PlayerDropItemEvent event, Player player, Item item, Location location, Block block) {
	}

	protected void onPlayerWalk(PlayerMoveEvent event, Player player, int i) {
	}

	protected void onPlayerTeleport(PlayerTeleportEvent event, Player player, Location location){
	}

	protected void onPlayerDeath(PlayerDeathEvent e, Player p){
	}

	protected void onPlayerConsume(PlayerItemConsumeEvent e, Player p, ItemStack i){
	}

	protected void onEntityDamage(EntityDamageEvent e, Entity entity){
	}

	protected void onPlayerDamage(EntityDamageEvent e, Player player){
	}

	protected void onSignChange(SignChangeEvent e, Player player, Sign sign){
	}

	protected void onEntitySpawn(EntitySpawnEvent e, Entity entity){
	}

	protected void onPrepareCraft(PrepareItemCraftEvent e){
	}

	protected void onItemCraft(CraftItemEvent e){
	}

	protected void onLogin(AsyncPlayerPreLoginEvent event){
	}

	protected void onBedEnter(PlayerBedEnterEvent event, Player player, Block block){
	}

	protected void onBedLeave(PlayerBedLeaveEvent event, Player player, Block block){
	}

	protected void onPlayerClick(PlayerInteractAtEntityEvent e, Player player, Player target){
	}

	protected void onPlayerXp(PlayerExpChangeEvent e, Player player, int amount){
	}

	protected void onPlayerXpMCMMO(McMMOPlayerXpGainEvent e, Player player, float rawXpGained){
	}

	protected void onShopSell(ShopPreTransactionEvent e, Player player, int amount, double price){
	}
}
