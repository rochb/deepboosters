package me.sharkz.deepbooster;

import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import me.sharkz.deepbooster.boosters.Booster;
import me.sharkz.deepbooster.boosters.BoostersManager;
import me.sharkz.deepbooster.commands.BoostCmd;
import me.sharkz.deepbooster.commands.MainCmd;
import me.sharkz.deepbooster.core.commands.CommandManager;
import me.sharkz.deepbooster.core.gson.GsonManager;
import me.sharkz.deepbooster.core.gson.adapters.BoosterAdapter;
import me.sharkz.deepbooster.core.inventories.InventoryManager;
import me.sharkz.deepbooster.core.listeners.ListenersManager;
import me.sharkz.deepbooster.core.utils.Logger;
import me.sharkz.deepbooster.core.utils.mc.Cuboid;
import me.sharkz.deepbooster.inventory.MainGUI;
import me.sharkz.deepbooster.listeners.BoosterListener;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;

public final class DeepBooster extends JavaPlugin {

    private static DeepBooster instance;
    public static final Logger LOG = new Logger("DEEP-BOOSTERS");
    public static String PREFIX;

    private static GsonManager gsonManager;
    private static InventoryManager inventoryManager;
    private static BoostersManager boostersManager;
    private static CommandManager commandManager;


    public DeepBooster() {
        instance = this;
    }

    @Override
    public void onEnable() {
        boostersManager = new BoostersManager();

        gsonManager = new GsonManager(this);
        gsonManager.add(new Lang());
        //gsonManager.add(new CooldownBuilder());
        gsonManager.add(boostersManager);
        gsonManager.load();

        PREFIX = ChatColor.translateAlternateColorCodes('&', Lang.msg.get("prefix"));

        commandManager = new CommandManager(this);
        commandManager.addCommand("deepbooster", new MainCmd());
        commandManager.addCommand("boost", new BoostCmd());
        commandManager.registerCommands();

        inventoryManager = new InventoryManager();
        inventoryManager.addInventory(1, new MainGUI());

        ListenersManager listenersManager = new ListenersManager(this);
        listenersManager.register(inventoryManager);
        listenersManager.register(new BoosterListener());

        boostersManager.update();
    }

    @Override
    public void onDisable() {
        gsonManager.save();
    }

    public static GsonBuilder getGsonBuilder() {
        return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
                .registerTypeHierarchyAdapter(Booster.class, new BoosterAdapter());
    }

    public static DeepBooster getInstance() {
        return instance;
    }

    public static InventoryManager getInventoryManager() {
        return inventoryManager;
    }

    public static BoostersManager getBoostersManager() {
        return boostersManager;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }
}
