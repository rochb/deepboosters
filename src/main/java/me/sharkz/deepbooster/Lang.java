package me.sharkz.deepbooster;

import me.sharkz.deepbooster.core.gson.Persist;
import me.sharkz.deepbooster.core.gson.Saveable;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Lang implements Saveable {

    public static Map<String, String> msg = new HashMap<>();
    public static String GUI_NAME = "&eDeep &aBooster";
    public static int GUI_SIZE = 36;
    public static Material GUI_FILLER = Material.LIGHT_GRAY_STAINED_GLASS_PANE;
    public static String GUI_FILLER_NAME = "";
    public static List<String> GUI_FILLER_LORE = Arrays.asList("&aHi !", "&bI'm a lore");
    public static String GUI_CLOSE_NAME = "&c&lCLOSE";
    public static List<String> GUI_CLOSE_LORE = Arrays.asList("&7Click here to", "&c&lclose&7 this menu.");

    public static String RIGHT_ANVIL_NAME = "&cTemporary boosters";
    public static List<String> RIGHT_ANVIL_LORE = Arrays.asList("Change me", "in the lang.json :)");
    public static String LEFT_ANVIL_NAME = "&cPermanent boosters";
    public static List<String> LEFT_ANVIL_LORE = Arrays.asList("Change me", "in the lang.json");

    public static String GUI_NONE_STRING = "None";
    public static String GUI_ACTIVE_BOOSTERS = "&bActive boosters :";

    {
        msg.put("prefix", "&b&lDEEP&7-&6&lBOOSTERS &7| &r");
        msg.put("no_permission", "&cOops... You don't have this permission !");
        msg.put("bad_use", "&eYou need to run the command like this&7: &a%command%");
        msg.put("xp_boost", "&a&lYou have received more xp thanks to your booster !");
        msg.put("mcmmo_boost", "&a&lYou have received more xp thanks to your booster !");
        msg.put("shop_boost", "&a&lYou sold your items $%bonus% more expensive thanks to your booster !");
        msg.put("booster_enabled", "&7You have enabled &a%booster%&7 booster for &a%player%&7 for &6%time%");
        msg.put("booster_start", "You have a %booster% booster for %time% !");
        msg.put("booster_stop", "Your %booster% booster has expired !");
        msg.put("offline_player", "&eThis player isn't online !");
        msg.put("invalid_booster", "&eThis booster dosen't exists !");
        msg.put("invalid_percent", "&ePlease specify a percentage between 1 & 100");
        msg.put("invalid_time", "&ePlease specify a valid time");
    }

    @Override
    public void save(Persist persist) {}

    @Override
    public void load(Persist persist) {
        persist.loadOrSaveDefault(this, Lang.class, "lang");
    }
}
