package me.sharkz.deepbooster.commands;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.boosters.Booster;
import me.sharkz.deepbooster.boosters.BoosterType;
import me.sharkz.deepbooster.core.commands.CommandType;
import me.sharkz.deepbooster.core.commands.VCommand;
import me.sharkz.deepbooster.core.utils.builders.TimerBuilder;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.EnumUtils;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatterBuilder;

import java.util.Arrays;

public class BoostGiveCmd extends VCommand {

    public BoostGiveCmd() {
        addSubCommand("give"); // 0
        addRequireArg("player"); // 1
        addRequireArg("booster"); // 2
        addRequireArg("percent"); // 3
        addRequireArg("time"); // 4
        addOptionalArg("time2");
        addOptionalArg("time3");
        addOptionalArg("time4");
        setPermission("deepbooster.boost");
    }

    @Override
    protected CommandType perform(JavaPlugin plugin) {
        long time = 0;
        if (args[4].equalsIgnoreCase("p") || args[4].equalsIgnoreCase("permanent"))
            time = 999999999;
        else {
            PeriodFormatterBuilder pfb = new PeriodFormatterBuilder();
            StringBuilder timeTmp = new StringBuilder();
            for (int i = 4; i < args.length; i++) {
                timeTmp.append(args[i]);
                if (args[i].contains("d"))
                    pfb.appendDays().appendSuffix("d");
                else if (args[i].contains("h"))
                    pfb.appendHours().appendSuffix("h");
                else if (args[i].contains("m"))
                    pfb.appendMinutes().appendSuffix("m");
                else if (args[i].contains("s"))
                    pfb.appendSeconds().appendSuffix("s");
            }
            Period p = pfb.toFormatter().parsePeriod(timeTmp.toString());
            if (p != null) time = p.toStandardSeconds().getSeconds();
        }
        Player target = Bukkit.getPlayer(args[1]);
        if (target == null || !target.isOnline()) {
            message(sender, Lang.msg.get("offline_player"));
            return CommandType.DEFAULT;
        }
        if (!StringUtils.isNumeric(args[3]) || Integer.parseInt(args[3]) < 1) {
            message(sender, Lang.msg.get("invalid_percent"));
            return CommandType.DEFAULT;
        }
        if (time == 0) {
            message(sender, Lang.msg.get("invalid_time"));
            return CommandType.DEFAULT;
        }

        BoosterType type = null;
        if(Arrays.stream(BoosterType.values()).anyMatch(boosterType -> boosterType.getName().equalsIgnoreCase(args[2])))
            type = Arrays.stream(BoosterType.values()).filter(boosterType -> boosterType.getName().equalsIgnoreCase(args[2])).findFirst().get();
        else if (Arrays.stream(BoosterType.values()).anyMatch(boosterType -> boosterType.getAliases().contains(args[2])))
            type = Arrays.stream(BoosterType.values()).filter(boosterType -> boosterType.getAliases().contains(args[2])).findFirst().get();

        if (type == null) {
            message(sender, Lang.msg.get("invalid_booster"));
            return CommandType.DEFAULT;
        }
        Booster booster = new Booster(type, target.getUniqueId(), time, Integer.parseInt(args[3]) - 100);
        DeepBooster.getBoostersManager().register(booster);
        String timeTxt = TimerBuilder.getStringTime(time);
        if (time == 999999999) timeTxt = "∞";
        message(sender, Lang.msg.get("booster_enabled").replace("%booster%", booster.getType().getName()).replace("%player%", target.getDisplayName()).replace("%time%", timeTxt));

        return CommandType.SUCCESS;
    }

}
