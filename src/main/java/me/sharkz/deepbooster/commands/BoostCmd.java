package me.sharkz.deepbooster.commands;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.boosters.Booster;
import me.sharkz.deepbooster.boosters.BoosterType;
import me.sharkz.deepbooster.core.commands.CommandType;
import me.sharkz.deepbooster.core.commands.VCommand;
import me.sharkz.deepbooster.core.utils.builders.TimerBuilder;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.EnumUtils;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class BoostCmd extends VCommand {

    public BoostCmd() {
        addSubCommand(DeepBooster.getCommandManager(), new BoostGiveCmd());
    }

    @Override
    protected CommandType perform(JavaPlugin plugin) {
        createInventory(DeepBooster.getInventoryManager(), player, 1);
        return CommandType.SUCCESS;
    }

}
