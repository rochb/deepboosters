package me.sharkz.deepbooster.commands;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.core.commands.CommandType;
import me.sharkz.deepbooster.core.commands.VCommand;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class MainCmd extends VCommand {

    @Override
    protected CommandType perform(JavaPlugin plugin) {
        sender.sendMessage(DeepBooster.PREFIX + "Version : 1.0");
        sender.sendMessage(DeepBooster.PREFIX + "Developed by : " + ChatColor.BOLD + ChatColor.GOLD + "Sharkz");
        return CommandType.SUCCESS;
    }
}
