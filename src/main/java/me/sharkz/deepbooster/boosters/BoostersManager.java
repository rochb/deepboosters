package me.sharkz.deepbooster.boosters;

import me.sharkz.deepbooster.DeepBooster;
import me.sharkz.deepbooster.core.gson.Persist;
import me.sharkz.deepbooster.core.gson.Saveable;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BoostersManager implements Saveable {

    private List<Booster> boosters = new ArrayList<>();

    public void register(Booster booster){
        if(boosters.contains(booster)) return;
        boosters.add(booster);
    }

    public void update(){
        new BukkitRunnable() {
            @Override
            public void run() {
                boosters.stream().filter(booster -> !booster.isRunning() && booster.getTime() < 999999999).forEach(booster -> {
                    booster.stop();
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            boosters.remove(booster);
                        }
                    }.runTask(DeepBooster.getInstance());
                });
            }
        }.runTaskTimerAsynchronously(DeepBooster.getInstance(), 20L, 20L);
    }

    public boolean hasBooster(Player player, BoosterType type){
        return boosters.stream().anyMatch(booster -> booster.isRunning() && booster.getUuid().equals(player.getUniqueId()) && booster.getType().equals(type));
    }

    public List<Booster> getActiveBoosters(Player player){
        return boosters.stream().filter(booster -> booster.isRunning() && booster.getUuid().equals(player.getUniqueId())).collect(Collectors.toList());
    }

    public List<Booster> getActiveBoosters(Player player, BoosterType type){
        return getActiveBoosters(player).stream().filter(booster -> booster.type.equals(type)).collect(Collectors.toList());
    }

    public List<Booster> getBoosters() {
        return boosters;
    }

    @Override
    public void save(Persist persist) {
        persist.save(this, "boosters");
    }

    @Override
    public void load(Persist persist) {
        persist.load(BoostersManager.class, "boosters");
    }
}
