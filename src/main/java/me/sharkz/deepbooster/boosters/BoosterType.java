package me.sharkz.deepbooster.boosters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum BoosterType {
    XP("Exp", Collections.singletonList("xp")),
    MCMMO("mcMMO"),
    SHOP("Shop", Collections.singletonList("sell"));

    private final String name;
    private final List<String> aliases;

    BoosterType(String name, List<String> aliases) {
        this.name = name;
        this.aliases = aliases;
    }

    BoosterType(String name) {
        this.name = name;
        this.aliases = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<String> getAliases() {
        return aliases;
    }
}
