package me.sharkz.deepbooster.boosters;

import me.sharkz.deepbooster.Lang;
import me.sharkz.deepbooster.core.utils.LUtils;
import me.sharkz.deepbooster.core.utils.builders.TimerBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Booster extends LUtils {

    private final UUID uuid;
    private final long start;
    private final long time;
    private final int percent;
    public final BoosterType type;

    public Booster(UUID uuid, long start, long time, int percent, BoosterType type){
        this.uuid = uuid;
        this.start = start;
        this.time = time;
        this.percent = percent;
        this.type = type;
        start();
    }

    public Booster(BoosterType type, UUID uuid, long time, int percent) {
        this.uuid = uuid;
        this.type = type;
        this.time = time;
        this.percent = percent;
        this.start = System.currentTimeMillis();
        start();
    }

    private void start(){
        String timeTxt = TimerBuilder.getStringTime(time);
        if(time == 999999999) timeTxt = "∞";
        message(getPlayer(), Lang.msg.get("booster_start").replace("%booster%", type.getName()).replace("%time%", timeTxt));
    }

    public void stop(){
        if(!getPlayer().isOnline()) return;
        message(getPlayer(), Lang.msg.get("booster_stop").replace("%booster%", type.getName()));
    }

    public boolean isRunning() {
        return getRemainingTime() >= 1;
    }

    public long getRemainingTime() {
        return (start + (time * 1000)) - System.currentTimeMillis();
    }

    public long getRemainingSeconds() {
        return ((start + (time * 1000)) - System.currentTimeMillis()) / 1000;
    }

    public long getTime() {
        return time;
    }

    public int getPercent() {
        return percent;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    public UUID getUuid() {
        return uuid;
    }

    public BoosterType getType() {
        return type;
    }

    public long getStart() {
        return start;
    }
}
